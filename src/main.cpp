#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <iostream>

__declspec(dllexport) unsigned long NvOptimusEnablement = 0x00000001;

int main()
{
	glfwInit();

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);

	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);

	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

	GLFWwindow* window = glfwCreateWindow(800, 600, "OpenGL Test Window", nullptr, nullptr);
    if (window == nullptr)
    {
	std::cout << "Failed to create GLFW window" << std::endl;
	glfwTerminate();
	return -1;
    } else
    {
        std::cout << "Init success" << std::endl;
    }
    glfwMakeContextCurrent(window);

    glewExperimental = GL_TRUE;

    if (glewInit() == GLEW_OK)
    {
        std::cout << "Success initialize GLEW" << std::endl;
    }

    int width, height;
    glfwGetFramebufferSize(window, &width, &height);

    glViewport(0, 0, width, height);

    glClearColor(0.5, 0.5, 0.5, 1);

    while(!glfwWindowShouldClose(window)) {
        glfwPollEvents();
        glClear(GL_COLOR_BUFFER_BIT);
        glfwSwapBuffers(window);
    }
    glfwTerminate();
	return 0;
}
